Django Cricket Management App

This is a Django based app in which we have a Cricket Team, Players, PlayerHistory, TeamMatch Table models in the core app of the project.
Data Creation and updating can be done from the admin panel.

I have created both REST APIs and template-based to which can be used as per your case.
All the APIs can be accessed by only authenticated users (Please make sure you are logged in before accessing these APIs).

Step 1:
	> create virtualenv
	> activate virtualenv
	> pip install -r requirements.txt
	> python manage.py runserver


APIs Endpoints are as following: 
1. Cricket Team List - http://127.0.0.1:8000/api/team-list/
2. Team Details(Includes the player) - http://127.0.0.1:8000/api/team-details/<int:team_id>/
3. Player Details(Based on Team) - http://127.0.0.1:8000/api/player/<int:player_id>/
4. Match Scheduled(Based on Team) - http://127.0.0.1:8000/api/match/<int:team_id>/
5. Points Table(Based on Team) - http://127.0.0.1:8000/api/points/<int:team_id>/
6. Team Match Scheduled List - http://127.0.0.1:8000/api/matches/
7. Team Points Table - http://127.0.0.1:8000/api/points/

Template URLs: 
1. Cricket Team List - http://127.0.0.1:8000/
2. Team Details(Includes the player) - http://127.0.0.1:8000/team-details/<int:team_id>/
3. Player Details(Based on Team) - http://127.0.0.1:8000/player/<int:player_id>/
4. Match Scheduled(Based on Team) - http://127.0.0.1:8000/match/<int:team_id>/
5. Points Table(Based on Team) - http://127.0.0.1:8000/points/<int:team_id>/
6. Team Match Scheduled List - http://127.0.0.1:8000/matches/
7. Team Points Table - http://127.0.0.1:8000/points/

When we create a team its auto schedule match with all teams.
