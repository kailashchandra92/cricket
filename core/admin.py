from django.contrib import admin
from .models import Team, Players, TeamMatch, PlayerHistory

# Register your models here.


admin.site.register(Team)
admin.site.register(Players)
admin.site.register(TeamMatch)
admin.site.register(PlayerHistory)
