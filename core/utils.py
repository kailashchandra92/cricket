from itertools import permutations
from core import models
from django.db.models import Q
import datetime
today = datetime.datetime.today()


def match_schedule_generator():
	"""
    It's creates schedule for all the teams present in the Cricket 
    Team table(each team plays twice against one team)
    """
	teams = models.Team.objects.all().order_by('?')
	if teams.count() > 1:
		matches_list = permutations(list(teams), 2)
		for match in matches_list:
			try:
				last = models.TeamMatch.objects.latest('id')
				match_date = last.match_date + datetime.timedelta(days=2)
			except models.TeamMatch.DoesNotExist:
				match_date = today + datetime.timedelta(days=2)

			case1 = models.TeamMatch.objects.filter(team=match[0], match_with=match[1])
			case2 = models.TeamMatch.objects.filter(team=match[1], match_with=match[0])
			if not case1.exists() and not case2.exists():
				models.TeamMatch.objects.create(team=match[0], match_with=match[1], match_date=match_date)


def get_points_list(teams):
	points_list = []
	for team in teams:
		team_match = models.TeamMatch.objects.filter(Q(team=team)|Q(match_with=team))
		team_dict = {
			'team': team.title, 'match': team_match.count(),
			'tied': team_match.filter(status='T').count(),
			'nr': team_match.filter(status='NR').count(),
			'won': team_match.filter(won_team=team).count()
		}
		result = team_dict['won'] + team_dict['nr'] + team_dict['tied']
		team_dict['lose'] = team_dict['match'] - result
		team_dict['pts'] = (team_dict['won'] * 2) + team_dict['tied']
		points_list.append(team_dict)
	return points_list
