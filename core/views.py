from core.models import Team, Players, TeamMatch, PlayerHistory
from django.views.generic import ListView, DetailView
from django.shortcuts import get_object_or_404
from django.db.models import Q
from core.utils import get_points_list
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from core.serializers import (TeamListSerializer, TeamSerializer, 
	HistorySerializer, TeamMatchSerializer)
from rest_framework.response import Response

# Create your views here.


class TeamListView(ListView):
	template_name = 'team-list.html'
	context_object_name = 'teams'
	model = Team


class TeamDetailView(DetailView):
	template_name = 'team-detail.html'
	model = Team

	def get_context_data(self, **kwargs):
		context = super(TeamDetailView, self).get_context_data(**kwargs)
		players_list = Players.objects.filter(team=self.object)
		context['players'] = players_list
		return context


class PlayerDetailView(DetailView):
	template_name = 'player.html'
	context_object_name = 'player'
	model = Players

	def get_context_data(self, **kwargs):
		context = super(PlayerDetailView, self).get_context_data(**kwargs)
		try:
			details = PlayerHistory.objects.get(player=self.object)
		except PlayerHistory.DoesNotExist:
			details = None
		context['details'] = details
		return context


class MatchView(ListView):
	template_name = 'matches.html'
	context_object_name = 'matches'
	model = TeamMatch

	def get_queryset(self):
		if self.kwargs:
			primary = self.kwargs['pk']
			lookup = Q(team=primary)|Q(match_with=primary)
			matches = TeamMatch.objects.filter(lookup)
		else:
			matches = TeamMatch.objects.all()
		return matches.order_by('match_date')

	def get_context_data(self, **kwargs):
		context = super(MatchView, self).get_context_data(**kwargs)
		if self.kwargs:
			context['team'] = get_object_or_404(Team, pk=self.kwargs['pk'])
		return context


class PointsView(ListView):
	template_name = 'points.html'
	model = Team

	def get_queryset(self):
		if self.kwargs:
			teams = Team.objects.filter(id=self.kwargs['pk'])
		else:
			teams = Team.objects.all()
		return teams

	def get_context_data(self, **kwargs):
		context = super(PointsView, self).get_context_data(**kwargs)
		if self.kwargs:
			context['team'] = get_object_or_404(Team, pk=self.kwargs['pk'])
		context['points'] = get_points_list(self.object_list)
		return context


# ===============================================================================
# ================================== API ========================================
# ===============================================================================


class ApiTeamListView(ListAPIView):
	permission_classes = (IsAuthenticated, )
	serializer_class = TeamListSerializer

	def list(self, request, *args, **kwargs):
		queryset = Team.objects.all()
		serializer = self.get_serializer(queryset, many=True)

		context = {'status': True, 'data': serializer.data}
		return Response(context, status=status.HTTP_200_OK)


class ApiTeamDetailView(ListAPIView):
	permission_classes = (IsAuthenticated, )
	serializer_class = TeamSerializer

	def list(self, request, *args, **kwargs):
		team = get_object_or_404(Team, pk=self.kwargs['pk'])
		players = Players.objects.filter(team=team)
		serializer = self.get_serializer(players, many=True)

		context = {
			'status': True, 
			'data': {
				'team': team.title, 
				'players': serializer.data
			}
		}
		return Response(context, status=status.HTTP_200_OK)


class ApiPlayerView(ListAPIView):
	permission_classes = (IsAuthenticated, )
	serializer_class = HistorySerializer

	def list(self, request, *args, **kwargs):
		player = get_object_or_404(Players, pk=self.kwargs['pk'])
		try:
			history = PlayerHistory.objects.get(player=player)
		except PlayerHistory.DoesNotExist:
			history = None
		serializer = self.get_serializer(history)

		context = {
			'status': True, 
			'data': {
				'team': player.team.title, 
				'players': serializer.data
			}
		}
		return Response(context, status=status.HTTP_200_OK)


class ApiMatchScheduleView(ListAPIView):
	permission_classes = (IsAuthenticated, )
	serializer_class = TeamMatchSerializer

	def list(self, request, *args, **kwargs):
		context = {'status': True, 'data': {}}
		if self.kwargs:
			primary = self.kwargs['pk']
			lookup = Q(team=primary)|Q(match_with=primary)
			matches = TeamMatch.objects.filter(lookup)
			team = get_object_or_404(Team, pk=self.kwargs['pk'])
			context['data']['team'] = team.title
		else:
			matches = TeamMatch.objects.all()

		matches = matches.order_by('match_date')
		serializer = self.get_serializer(matches, many=True)
		context['data']['matches'] = serializer.data
		return Response(context, status=status.HTTP_200_OK)


class ApiPointsView(ListAPIView):
	permission_classes = (IsAuthenticated, )
	serializer_class = TeamMatchSerializer

	def list(self, request, *args, **kwargs):
		context = {'status': True, 'data': {}}
		if self.kwargs:
			teams = Team.objects.filter(id=self.kwargs['pk'])
			team = get_object_or_404(Team, pk=self.kwargs['pk'])
			context['data']['team'] = team.title
		else:
			teams = Team.objects.all()

		context['data']['points'] = get_points_list(teams)
		return Response(context, status=status.HTTP_200_OK)
