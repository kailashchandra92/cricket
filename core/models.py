from django.db import models
from django.db.models import signals
from core.utils import (match_schedule_generator)
import datetime

# Create your models here.


class Team(models.Model):
	title = models.CharField(max_length=100)
	logo = models.ImageField(upload_to='logo')
	club_state = models.CharField(max_length=100)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	@property
	def full_path(self):
		return self.logo.url

	def __str__(self):
		return self.title


class Players(models.Model):
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100, null=True, blank=True)
	image = models.ImageField(upload_to='players')
	team = models.ForeignKey(Team, db_index=True, on_delete=models.PROTECT)
	jersey_number = models.PositiveIntegerField(unique=True)
	country = models.CharField(max_length=100)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	@property
	def full_name(self):
		last_name = ''
		if self.last_name is not None:
			last_name = f' {self.last_name}'
		return f'{self.first_name}{last_name}'

	@property
	def full_path(self):
		return self.image.url

	def __str__(self):
		return self.first_name


class PlayerHistory(models.Model):
	player = models.ForeignKey(Players, db_index=True, on_delete=models.PROTECT)
	total_match = models.PositiveIntegerField(default=0)
	total_run = models.PositiveIntegerField(default=0)
	total_centuries = models.PositiveIntegerField(default=0)
	total_fifties = models.PositiveIntegerField(default=0)
	highest_score = models.PositiveIntegerField(default=0)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		player = 'NULL-NULL'
		if self.player:
			player = self.player.full_name
		return player


class TeamMatch(models.Model):
	MATCH_STATUS = (
		('W', 'Won'),
		# ('L', 'Lost'),
		('T', 'Tied'),
		('NR', 'NR')
	)

	team = models.ForeignKey(Team, on_delete=models.PROTECT, related_name='rel_team')
	match_with = models.ForeignKey(Team, on_delete=models.PROTECT, related_name='with_team')
	status = models.CharField(choices=MATCH_STATUS, max_length=10, blank=True, null=True)
	won_team = models.ForeignKey(
		Team, on_delete=models.PROTECT, related_name='won_team', blank=True, null=True)
	match_date = models.DateField(blank=True, null=True)
	match_time = models.TimeField(default=datetime.time(17, 00))
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def __str__(self):
		team_match = 'NULL-NULL'
		if self.team and self.match_with:
			team_match = f'{self.team.title}-{self.match_with.title}'
		return team_match


def create_team_match(sender, instance, created, **kwargs):
    if created:
        match_schedule_generator()

signals.post_save.connect(receiver=create_team_match, sender=Team)
