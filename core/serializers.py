from rest_framework import serializers
from core.models import Team, Players, PlayerHistory, TeamMatch


class TeamListSerializer(serializers.ModelSerializer):
    
	class Meta:
		model = Team
		fields = ('id', 'title', 'logo')


class TeamSerializer(serializers.ModelSerializer):
    
	class Meta:
		model = Players
		fields = ('id', 'first_name', 'last_name', 'image')


class HistorySerializer(serializers.ModelSerializer):
	player_name = serializers.SerializerMethodField()

	def get_player_name(self,obj):
		return obj.player.full_name

	class Meta:
		model = PlayerHistory
		fields = ('player_name', 'id', 'total_match', 'total_run', 'highest_score', 
			'total_centuries', 'total_fifties')


class TeamMatchSerializer(serializers.ModelSerializer):
	details = serializers.SerializerMethodField()

	def get_details(self,obj):
		return f'{obj.team} VS {obj.match_with}'

	class Meta:
		model = TeamMatch
		fields = ('id', 'match_date', 'details', 'match_time')
