from django.urls import path
from django.views.generic import TemplateView
from core import views

app_name = 'core'

urlpatterns = [
    path('', views.TeamListView.as_view(), name='teams'),
	path('team-details/<int:pk>/', views.TeamDetailView.as_view(), name='details'),
	path('player/<int:pk>/', views.PlayerDetailView.as_view(), name='player'),
	path('matches/', views.MatchView.as_view(), name='matches'),
	path('match/<int:pk>/', views.MatchView.as_view(), name='team-match'),
	path('points/', views.PointsView.as_view(), name='points'),
	path('points/<int:pk>/', views.PointsView.as_view(), name='team-point'),

	# ===============================================================================
	# ================================== API ========================================
	# ===============================================================================

	path('api/team-list/', views.ApiTeamListView.as_view(), name='api-teams'),
	path('api/team-details/<int:pk>/', views.ApiTeamDetailView.as_view(), name='api-details'),
	path('api/player/<int:pk>/', views.ApiPlayerView.as_view(), name='api-player'),
	path('api/matches/', views.ApiMatchScheduleView.as_view(), name='api-match'),
	path('api/match/<int:pk>/', views.ApiMatchScheduleView.as_view(), name='api-team-match'),
	path('api/points/', views.ApiPointsView.as_view(), name='api-points'),
	path('api/points/<int:pk>/', views.ApiPointsView.as_view(), name='api-team-point'),
]
